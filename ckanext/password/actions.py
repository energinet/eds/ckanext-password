# -*- coding: utf-8 -
import logging
import ckan.logic as l
import ckanext.password.helpers as _h
from ckan.common import _
from ckanext.eds.model.user_extra import UserExtra

try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

log = logging.getLogger(__name__)


def user_create(context, data_dict):
    password = data_dict.get('password1', '')
    if 'password' in data_dict:
        password = data_dict['password']

    valid = _h.validate_password(password)
    if not valid:
        msg = config.get('ckanext.password.invalid_password_message', None)
        if msg is None:
            msg = _('Password must consist of at least 8 characters and at least three of following four character \
                     types: Uppercase letter, Lowercase letter, Number, Special characters: !, @, #, $, %, ^, &, *.')
        raise l.ValidationError({'password': [msg]})

    consent = data_dict.get('privacypolicyconsent', '')
    if not _h.validate_privacy_consent(consent):
        raise l.ValidationError({'privacypolicyconsent': [_('Missing consent')]})

    user = l.action.create.user_create(context, data_dict)
    user_extra = UserExtra.get(user['id'], 'privacypolicyconsent')
    if user_extra:
        user_extra.key = 'privacypolicyconsent'
        user_extra.value = 'true'
        user_extra.save()
    else:
        user_extra = UserExtra(
            user_id=user['id'],
            key='privacypolicyconsent',
            value='true',
            state='active'
        )
        user_extra.save()

    return user

    


def user_update(context, data_dict):
    password = data_dict.get('password1', '')
    if 'password' in data_dict:
        password = data_dict['password']

    if password != '':
        valid = _h.validate_password(password)
        if not valid:
            msg = config.get('ckanext.password.invalid_password_message', None)
            if msg is None:
                msg = _('Password must consist of at least 8 characters and at least three of following four character \
                     types: Uppercase letter, Lowercase letter, Number, Special characters: !, @, #, $, %, ^, &, *.')
            raise l.ValidationError({'password': [msg]})
    return l.action.update.user_update(context, data_dict)
