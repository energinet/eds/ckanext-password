import os
import redis
from ckan.common import config

class RedisClient(object):
    prefix = ''

    def __init__(self):
        if config.get('testing'):
            host = config.get('ckan.redis.host')
            port = config.get('ckan.redis.port')
            db = config.get('ckan.redis.db')
            password = ''
            ssl = False
        else: # In production we use Azure specific redis URI:
            redis_url = os.environ['CKAN_REDIS_URL']
            redis_url_parts = redis_url.split('@')
            host = redis_url_parts[1].split(':')[0]
            password = redis_url_parts[0][10:]
            port = redis_url_parts[1].split(':')[1].split('/')[0]
            db = redis_url_parts[1].split(':')[1].split('/')[1]
            ssl = True
        self.client = redis.StrictRedis(host=host, port=port, db=db,
            password=password, ssl=ssl)

    def get(self, key):
        return self.client.get(self.prefix + key)

    def set(self, key, value):
        return self.client.set(self.prefix + key, value)

    def delete(self, key):
        return self.client.delete(self.prefix + key)

class CSRFClient(RedisClient):
    prefix = 'security_csrf_'

class ThrottleClient(RedisClient):
    prefix = 'security_throttle_'
